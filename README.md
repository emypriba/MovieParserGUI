# Movie Parser GUI

## Info

Adding movies to database and getting requested movies by some criteria in GUI.  Based on movie-parser project on GitLab.

## Quickstart
- `python movie-parser.gui.py`
