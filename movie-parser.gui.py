import json
import sys
import urllib.request
from functools import partial

from PySide2 import QtGui
from PySide2.QtCore import Qt
from PySide2.QtWidgets import (
    QApplication,
    QGridLayout,
    QLabel,
    QLineEdit,
    QListWidget,
    QMainWindow,
    QMessageBox,
    QPushButton,
    QScrollArea,
    QTabWidget,
    QVBoxLayout,
    QWidget,
)

from helpers import (
    add_movie,
    check_endpoint,
    get_movie_data,
    get_requested_movies,
    is_online,
)


class MovieTabs(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Movie Parser UI")
        self.setFixedSize(600, 600)
        vbox = QVBoxLayout()
        tab_widget = QTabWidget()
        tab_widget.setFont(QtGui.QFont("Sanserif", 12))
        tab_widget.addTab(AddMovie(), "Add movie")
        tab_widget.addTab(GetFilteredMovies(), "Get filtered movie(s)")
        vbox.addWidget(tab_widget)
        self.version_label = QLabel()
        self.warning_label = QLabel()
        self.message_box = QMessageBox()
        vbox.addWidget(self.version_label)
        vbox.addWidget(self.warning_label)
        self.setLayout(vbox)
        self.setup_ui()
        self.check_requirements()

    def setup_ui(self):
        self.version_label.setText("v1.0.0")
        self.version_label.setFont(QtGui.QFont("Times New Roman", 12))
        self.version_label.setStyleSheet("color:black")
        self.version_label.setAlignment(Qt.AlignRight)
        self.warning_label.setFont(QtGui.QFont("Times New Roman", 12))
        self.warning_label.setStyleSheet("color:black")
        self.warning_label.setAlignment(Qt.AlignCenter)

    def check_requirements(self):
        if not check_endpoint():
            self.warning_label.setText("Endpoint offline!")
        elif not is_online():
            self.warning_label.setText("Check your connection!")
        else:
            self.warning_label.setText("Ready!")


class AddMovie(QWidget):
    def __init__(self):
        super().__init__()
        movie_id = QLabel("Movie id: ")
        self.movie_id_edit = QLineEdit()
        self.movie_id_edit.setPlaceholderText("Put positive whole number!")
        add_movie_button = QPushButton("Add movie")
        info = QLabel()
        url_link = '<a href="https://developers.themoviedb.org/3">More info</a>'
        info.setText(url_link)
        info.setOpenExternalLinks(True)
        filtering_grid = QGridLayout()
        filtering_grid.addWidget(movie_id, 0, 0)
        filtering_grid.addWidget(self.movie_id_edit, 0, 1, 1, 2)
        filtering_grid.addWidget(add_movie_button, 2, 1, alignment=Qt.AlignTop)
        filtering_grid.addWidget(info, 3, 0, 1, 3, alignment=Qt.AlignCenter)
        self.setLayout(filtering_grid)
        add_movie_button.clicked.connect(self.create_messagebox)

    def create_messagebox(self):
        message_box = QMessageBox()
        message_box.setWindowTitle("Info")
        message = add_movie(self.movie_id_edit.text())
        m = json.loads(message)
        message_box.setStandardButtons(QMessageBox.Ok)
        message_box.setText(message)
        message_box.setIcon(QMessageBox.Icon.Information)
        if m["message"] == "Created":
            message_box.setText(f"Movie {self.movie_id_edit.text()} entry created!")
        elif m["message"] == "Not Found":
            message_box.setText(f"Movie {self.movie_id_edit.text()} not found!")
        elif m["message"] == "Bad Request":
            message_box.setText(f"Invalid movie id format {self.movie_id_edit.text()}!")
        else:
            message_box.setText(f"Movie {self.movie_id_edit.text()} already exists!")
        message_box.exec_()


class GetFilteredMovies(QMainWindow):
    def __init__(self):
        super().__init__()
        self.genres = QLabel("Genres: ")
        self.genres_edit = QLineEdit()
        self.popularity = QLabel("Popularity: ")
        self.popularity_edit = QLineEdit()
        self.vote_average = QLabel("Vote average: ")
        self.vote_average_edit = QLineEdit()
        self.title = QLabel("Title: ")
        self.title_edit = QLineEdit()
        self.set_ui()

    def set_ui(self):
        self.genres = QLabel("Genres: ")
        self.genres_edit = QLineEdit()
        self.genres_edit.setPlaceholderText("Put strings separated by comma!")
        self.popularity = QLabel("Popularity: ")
        self.popularity_edit = QLineEdit()
        self.popularity_edit.setPlaceholderText("Put number from 0 to 100!")
        self.vote_average = QLabel("Vote average: ")
        self.vote_average_edit = QLineEdit()
        self.vote_average_edit.setPlaceholderText("Put number from 0 to 10!")
        self.title = QLabel("Title: ")
        self.title_edit = QLineEdit()
        self.title_edit.setPlaceholderText("Put title here...")
        filter_button = QPushButton("Filter")
        filtering_grid = QGridLayout()
        filtering_grid.addWidget(self.genres, 0, 0)
        filtering_grid.addWidget(self.genres_edit, 0, 1, 1, 2)
        filtering_grid.addWidget(self.popularity, 1, 0)
        filtering_grid.addWidget(self.popularity_edit, 1, 1, 1, 2)
        filtering_grid.addWidget(self.vote_average, 2, 0)
        filtering_grid.addWidget(self.vote_average_edit, 2, 1, 1, 2)
        filtering_grid.addWidget(self.title, 3, 0)
        filtering_grid.addWidget(self.title_edit, 3, 1, 1, 2)
        filtering_grid.addWidget(filter_button, 4, 1, alignment=Qt.AlignTop)
        filter_movies_widget = QWidget()
        filter_movies_widget.setLayout(filtering_grid)
        self.setCentralWidget(filter_movies_widget)
        filter_button.clicked.connect(self.filter_movie_db)

    def filter_movie_db(self):
        try:
            data = get_requested_movies(
                self.genres_edit.text(),
                self.popularity_edit.text(),
                self.vote_average_edit.text(),
                self.title_edit.text(),
            )
            if data:
                filtered_movies_grid = QGridLayout()
                back_button = QPushButton("Back")
                rw = 7
                for d in data:
                    details_button = QPushButton("Details")
                    filtered_movies_grid.addWidget(self.genres, 0, 0, 1, 2)
                    filtered_movies_grid.addWidget(self.genres_edit, 0, 2, 1, 5)
                    filtered_movies_grid.addWidget(self.popularity, 1, 0, 1, 2)
                    filtered_movies_grid.addWidget(self.popularity_edit, 1, 2, 1, 5)
                    filtered_movies_grid.addWidget(self.vote_average, 2, 0, 1, 2)
                    filtered_movies_grid.addWidget(self.vote_average_edit, 2, 2, 1, 5)
                    filtered_movies_grid.addWidget(self.title, 3, 0)
                    filtered_movies_grid.addWidget(self.title_edit, 3, 2, 1, 5)
                    filtered_movies_grid.addWidget(
                        back_button, 4, 0, 1, 7, alignment=Qt.AlignCenter
                    )
                    title_label = QLabel("Showing results")
                    movie_id_label = QLabel("Movie id")
                    movie_title_label = QLabel("Title")
                    popularity_label = QLabel("Popularity")
                    vote_average_label = QLabel("Vote average")
                    vote_count_label = QLabel("Vote count")
                    genres_label = QLabel("Genres")
                    button_label = QLabel("About")
                    movie_id_label_results = QLabel()
                    movie_title_label_results = QLabel()
                    popularity_label_results = QLabel()
                    vote_average_label_results = QLabel()
                    vote_count_label_results = QLabel()
                    genres_label_results = QLabel()
                    filtered_movies_grid.addWidget(
                        title_label, 5, 0, 1, 7, alignment=Qt.AlignCenter
                    )
                    filtered_movies_grid.addWidget(
                        movie_id_label, 6, 0, alignment=Qt.AlignCenter
                    )
                    filtered_movies_grid.addWidget(
                        movie_title_label, 6, 1, alignment=Qt.AlignCenter
                    )
                    filtered_movies_grid.addWidget(
                        popularity_label, 6, 2, alignment=Qt.AlignCenter
                    )
                    filtered_movies_grid.addWidget(
                        vote_average_label, 6, 3, alignment=Qt.AlignCenter
                    )
                    filtered_movies_grid.addWidget(
                        vote_count_label, 6, 4, alignment=Qt.AlignCenter
                    )
                    filtered_movies_grid.addWidget(
                        genres_label, 6, 5, alignment=Qt.AlignCenter
                    )
                    filtered_movies_grid.addWidget(
                        button_label, 6, 6, alignment=Qt.AlignCenter
                    )
                    filtered_movies_grid.addWidget(movie_id_label_results, rw, 0)
                    movie_id_label_results.setText(str(d["movie_id"]))
                    filtered_movies_grid.addWidget(movie_title_label_results, rw, 1)
                    movie_title_label_results.setText(d["title"])
                    filtered_movies_grid.addWidget(popularity_label_results, rw, 2)
                    popularity_label_results.setText(d["popularity"])
                    filtered_movies_grid.addWidget(vote_average_label_results, rw, 3)
                    vote_average_label_results.setText(d["vote_average"])
                    filtered_movies_grid.addWidget(vote_count_label_results, rw, 4)
                    vote_count_label_results.setText(str(d["vote_count"]))
                    filtered_movies_grid.addWidget(genres_label_results, rw, 5)
                    genres_label_results.setText(str(", ".join(d["genres"])))
                    filtered_movies_grid.addWidget(details_button, rw, 6)
                    details_button.setText("Details")
                    details_button.clicked.connect(
                        partial(self.requested_movie_details, d["movie_id"])
                    )
                    rw += 1
                filtered_movies_widget = QWidget()
                filtered_movies_widget.setLayout(filtered_movies_grid)
                filtered_movies_scroll_bar = QScrollArea()
                filtered_movies_scroll_bar.setHorizontalScrollBarPolicy(
                    Qt.ScrollBarAsNeeded
                )
                filtered_movies_scroll_bar.setVerticalScrollBarPolicy(
                    Qt.ScrollBarAsNeeded
                )
                filtered_movies_scroll_bar.setWidgetResizable(True)
                filtered_movies_scroll_bar.setWidget(filtered_movies_widget)
                self.setCentralWidget(filtered_movies_scroll_bar)
                self.setGeometry(600, 600, 1500, 1500)
                self.show()
                back_button.clicked.connect(self.set_ui)
            else:
                message_box = QMessageBox()
                message_box.setWindowTitle("Info")
                message_box.setStandardButtons(QMessageBox.Ok)
                message_box.setText("No movie found! Try again!")
                message_box.exec_()
        except json.decoder.JSONDecodeError:
            data = get_requested_movies(
                self.genres_edit.text(),
                self.popularity_edit.text(),
                self.vote_average_edit.text(),
                self.title_edit.text(),
            )
            message_box = QMessageBox()
            message_box.setText(data)
            message_box.setIcon(QMessageBox.Icon.Question)
            message_box.exec_()

    def requested_movie_details(self, movie_id):
        mov_id_result = QListWidget()
        title_label_result = QLabel()
        movie_id_label_result = QLabel()
        movie_title_label_result = QLabel()
        popularity_label_result = QLabel()
        vote_average_label_result = QLabel()
        vote_count_label_result = QLabel()
        genres_label_result = QLabel()
        overview_label_result = QLabel()
        release_date_label_result = QLabel()
        imdb_id_label_result = QLabel()
        image_label_result = QLabel()
        image_result = QtGui.QPixmap()
        data = json.loads(get_movie_data(movie_id))
        back_button = QPushButton("Back")
        requested_movie_grid = QGridLayout()
        requested_movie_grid.addWidget(
            back_button, 1, 0, 1, 2, alignment=Qt.AlignCenter
        )
        title_label_result.setText(f'Showing result for {data["movie_id"]}')
        title_label_result.setFont(QtGui.QFont("Times New Roman", 15))
        movie_id_label_result.setText(f'Movie id: {data["movie_id"]}')
        movie_title_label_result.setText(f'Title: {data["title"]}')
        popularity_label_result.setText(f'Popularity: {data["popularity"]}')
        vote_average_label_result.setText(f'Vote average: {data["vote_average"]}')
        vote_count_label_result.setText(f'Vote count: {data["vote_count"]}')
        genres_label_result.setText(f'Genres: {", ".join(data["genres"])}')
        url = "https://image.tmdb.org/t/p/w200%s" % data["poster_path"]
        file = urllib.request.urlopen(url).read()
        image_result.loadFromData(file)
        image_label_result.setPixmap(image_result)
        overview_label_result.setText(f'Overview: {data["overview"]}')
        release_date_label_result.setText(f'Release date: {data["release_date"]}')
        imdb_id_label_result.setText(f'Imdb id: {data["imdb_id"]}')
        if data["movie_id"]:
            mov_id_result.addItems(data)
        else:
            mov_id_result.addItems(["No movie found"])
        requested_movie_grid.addWidget(
            title_label_result, 2, 0, alignment=Qt.AlignCenter
        )
        requested_movie_grid.addWidget(movie_id_label_result, 3, 0)
        requested_movie_grid.addWidget(movie_title_label_result, 4, 0)
        requested_movie_grid.addWidget(popularity_label_result, 5, 0)
        requested_movie_grid.addWidget(vote_average_label_result, 6, 0)
        requested_movie_grid.addWidget(vote_count_label_result, 7, 0)
        requested_movie_grid.addWidget(genres_label_result, 8, 0)
        requested_movie_grid.addWidget(image_label_result, 9, 0)
        requested_movie_grid.addWidget(overview_label_result, 10, 0)
        requested_movie_grid.addWidget(release_date_label_result, 11, 0)
        requested_movie_grid.addWidget(imdb_id_label_result, 12, 0)
        requested_movie_widget = QWidget()
        requested_movie_widget.setLayout(requested_movie_grid)
        requested_movie_scroll_bar = QScrollArea()
        requested_movie_scroll_bar.setHorizontalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        requested_movie_scroll_bar.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        requested_movie_scroll_bar.setWidgetResizable(True)
        requested_movie_scroll_bar.setWidget(requested_movie_widget)
        self.setCentralWidget(requested_movie_scroll_bar)
        self.setGeometry(800, 800, 2000, 1500)
        self.show()
        back_button.clicked.connect(self.set_ui)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    tab_dialog = MovieTabs()
    tab_dialog.show()
    sys.exit(app.exec_())
