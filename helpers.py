from urllib.parse import urlencode

import http.client as client
import json
import socket
import urllib.error
import urllib.request


API_PORT = 5000
API_HOST = "0.0.0.0"


def get_movie_data(movie_id):
    try:
        connection = client.HTTPConnection(API_HOST, API_PORT, timeout=1)
        connection.request("GET", f"/movies/{movie_id}")
        response = connection.getresponse()
        connection.close()
        if response.code == 404:
            raise client.HTTPException
        data = response.read().decode("U8")
        if data:
            return data
    except client.HTTPException:
        return "Movie id not in base, please try again"
    except (ConnectionRefusedError, socket.timeout):
        return "Connection refused"


def add_movie(movie_id):
    url = f"{API_HOST}:{API_PORT}"
    params = json.dumps({"movie_id": movie_id})
    headers = {"Content-type": "application/json"}
    conn = client.HTTPConnection(url)
    conn.request("POST", "/movies", params, headers)
    response = conn.getresponse()
    msg = response.read().decode()
    return msg


def get_requested_movies(genres, popularity, vote_average, title):
    try:
        query_params = {
            "genres": genres,
            "popularity": popularity,
            "vote_average": vote_average,
            "title": title,
        }
        query_string = urlencode(query_params)
        connection = client.HTTPConnection(API_HOST, API_PORT, timeout=1)
        connection.request("GET", f"/movies?{query_string}")
        response = connection.getresponse()
        connection.close()
        if response.code != 200:
            raise client.HTTPException
        movie = json.loads(response.read().decode("U8"))
        return movie
    except (client.HTTPException, socket.timeout):
        return 0


def is_online(host="http://google.com"):
    try:
        urllib.request.urlopen(host, timeout=3)
        return True
    except urllib.request.URLError:
        return False
    except ConnectionRefusedError:
        return False


def check_endpoint():
    try:
        response = client.HTTPConnection(API_HOST, API_PORT, timeout=1)
        response.request("GET", "/movies/123")
        return True
    except (urllib.request.URLError, ConnectionRefusedError):
        return False
